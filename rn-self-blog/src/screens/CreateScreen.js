import React, {useState, useRef} from "react";
import {Text, View, StyleSheet, TextInput, Image, Button, ScrollView, TouchableWithoutFeedback, Keyboard} from "react-native";
import {HeaderButtons, Item} from "react-navigation-header-buttons";
import {AppHeaderIcon} from "../components/AppHeaderIcon";
import {THEME} from "../theme";
import {useDispatch} from "react-redux";
import {addPost} from "../store/actions/post";
import {PhotoPicker} from "../components/PhotoPicker";

export const CreateScreen = ({navigation}) => {
    const dispatch = useDispatch()

    const [text, setText] = useState('')

    //чтобы не вызывать ререндер, можем юзать useRef(вызов -> получаем константу в рамках реакта)
    //если будем её менять, она не будет вызывать ререндер компонента => повышаем производительность компонента
    const imgRef = useRef();

    const saveHandler = () => {
        const post = {
            date: new Date().toJSON(),
            text: text,
            img: imgRef.current,
            booked: false
        }
        console.log("poST", post)
        dispatch(addPost(post))
        navigation.navigate('Main')
    }

    const photoPickHandler = (uri) => {
        imgRef.current = uri
    }

    return (
        <ScrollView>
            <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
                <View style={styles.wrapper}>
                    <Text style={styles.title}>Создай новый пост</Text>
                    <TextInput style={styles.textarea}
                               placeholder='Введите текст поста'
                               value={text}
                               onChangeText={setText}
                               multiline
                    />
                    <PhotoPicker onPick={photoPickHandler}/>
                    <Button title='Создать пост'
                            color={THEME.MAIN_COLOR}
                            disabled={!text || !imgRef.current}
                            onPress={saveHandler}/>
                </View>
            </TouchableWithoutFeedback>
        </ScrollView>
    )
}

CreateScreen.navigationOptions = ({navigation}) => ({ //мы не можем засунуть внутри объекта navigation, потому что это объект =>
    //превращаем в функцию
    headerTitle: 'Создать пост',
    headerLeft: () =>
        (<HeaderButtons HeaderButtonComponent={AppHeaderIcon}>
            <Item title="Toggle Drawer"
                  iconName="ios-menu"
                  onPress={() => navigation.toggleDrawer()}/>
        </HeaderButtons>)
})

const styles = StyleSheet.create({
    wrapper: {
        padding: 10
    },
    title: {
        fontSize: 20,
        textAlign: 'center',
        fontFamily: 'open-regular',
        marginVertical: 10
    },
    textarea: {
        padding: 10,
        marginBottom: 10 //для того чтобы элементы которые снизу имели отступ в 10 пикселей от инпута
    }
})
