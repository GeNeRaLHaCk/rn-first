import React, {useEffect, useCallback} from "react";
import {DATA} from "../data";
import {THEME} from "../theme";
import {HeaderButtons, Item} from "react-navigation-header-buttons";
import {AppHeaderIcon} from "../components/AppHeaderIcon";
import {Alert, Button, Image, ScrollView, Text, View} from "react-native";
import {StyleSheet} from "react-native";
import {useDispatch, useSelector} from "react-redux";
import {removePost, toggleBooked} from "../store/actions/post";

export const PostScreen = ({navigation}) => {
    const dispatch = useDispatch()

    const postId = navigation.getParam('postId')

    const post = useSelector(state => state.post.allPosts.find(p => p.id === postId))

    const booked = useSelector(state =>
        state.post.bookedPosts.some(post => post.id === postId))//определяем есть ли хоть 1 объект по такому условию

    useEffect(() => {
        navigation.setParams({booked})
    }, [booked]) //в случае если флаг измениться, будем задавать параметром навигации

    //за счёт того что что мы обернули ф-ию в хук useCallback -> React не будет
    // менять или создавать новую ф-ию при рендере (useEffect будет вызван 1 раз)
    const toggleHandler = useCallback(() => { //в useCallback передаём ф-ию которую хотим создать
        dispatch(toggleBooked(post))
    }, [dispatch, post])//далее зависимости

    useEffect(() => { // выполняется после рендера компонента (когда компонент готов к работе)
        navigation.setParams({toggleHandler}) //toggle handler может быть зациклен(чтобы не было, оборачиваем в useCallback)
    }, [toggleHandler])

    const removeHandler = () => {
        Alert.alert(
            "Удаление поста",
            "Вы точно хотите удалить пост?",
            [
                {
                    text: "Отменить",
                    style: "cancel"
                },
                {
                    text: "OK",
                    style: 'destructive',
                    onPress() {
                        navigation.navigate('Main')
                        dispatch(removePost(postId))
                    }
                }
            ]
        );
    }

    if (!post) {
        return null;
    }
    return (
        <ScrollView>
            <Image source={{uri: post.img}} style={styles.image}/>
            <View style={styles.textWrap}>
                <Text style={styles.title}>{post.text}</Text>
            </View>
            <Button title='Delete' color={THEME.DANGER_COLOR} onPress={removeHandler}/>
        </ScrollView>
    )
}

//пользоваться прелесятми redux можно исключительно в компонентах

PostScreen.navigationOptions = ({navigation}) => { //для того чтобы отображать динамические значения, можем просто превратить в функцию
    const postId = navigation.getParam('postId')
    const date = navigation.getParam('date')
    const booked = navigation.getParam('booked')
    const toggleHandler = navigation.getParam('toggleHandler')

    const iconName = booked ? 'ios-star' : 'ios-star-outline'
    return {
        headerTitle: 'Post№' + postId + ' от ' + new Date(date).toLocaleDateString(),
        headerStyle: {
            backgroundColor: 'red',
        },
        headerTintColor: '#fff',
        headerRight: () =>
            (<HeaderButtons HeaderButtonComponent={AppHeaderIcon}>
                <Item title="Take photo"
                      iconName={iconName}
                      onPress={toggleHandler}/>
            </HeaderButtons>),
    }
}

const styles = StyleSheet.create({
    image: {
        width: '100%',
        height: 200
    },
    textWrap: {
        padding: 10
    },
    title: {
        fontFamily: 'open-regular'
    }
})
