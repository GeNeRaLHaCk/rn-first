import React from "react";
import {HeaderButtons, Item} from 'react-navigation-header-buttons'
import {useSelector} from "react-redux";
import {Post} from "../components/Post";
import {AppHeaderIcon} from "../components/AppHeaderIcon";
import {PostList} from "../components/PostList";
//по умолчанию открывается MainScreen, а мы уже загрузили туда данные, так что нужен только useSelector
export const BookedScreen = ({navigation}) => {
    const openPostHandler = (post) => {
        navigation.navigate('Post', {postId: post.id, date: post.date, booked: post.booked})
    }

    const bookedPosts = useSelector(state => state.post.bookedPosts)
    //соединили наш функциональный компонент со store
    //можно юзать ещё компоненты которые образованы через class, но для этого есть ф-ия connect(которая позволит соединять)
    //mapDispatchToProps и прочее чтобы писать в классическом виде
    return <PostList data={bookedPosts}
                     onOpen={openPostHandler}/>
}

BookedScreen.navigationOptions = ({navigation}) => ({
    headerTitle: 'Избранное',
    headerLeft: () =>
        (<HeaderButtons HeaderButtonComponent={AppHeaderIcon}>
            <Item title="Toggle Drawer"
                  iconName="ios-menu"
                  onPress={() => navigation.toggleDrawer()}/>
        </HeaderButtons>)
})

