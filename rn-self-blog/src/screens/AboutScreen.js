import React from "react";
import {Text, View, StyleSheet} from "react-native";
import {HeaderButtons, Item} from "react-navigation-header-buttons";
import {AppHeaderIcon} from "../components/AppHeaderIcon";
import {MainScreen} from "./MainScreen";

export const AboutScreen = ({}) => {
    return (
        <View style={styles.center}>
            <Text>Это лучшее приложение для личных заметок</Text>
            <Text>Версия приложения <Text style={styles.version}>1.0.0</Text></Text>
        </View>
    )
}

AboutScreen.navigationOptions = ({navigation}) => ({ //мы не можем засунуть внутри объекта navigation, потому что это объект =>
    //превращаем в функцию
    headerTitle: 'О приложении',
    headerLeft: () =>
        (<HeaderButtons HeaderButtonComponent={AppHeaderIcon}>
            <Item title="Toggle Drawer"
                  iconName="ios-menu"
                  onPress={() => navigation.toggleDrawer()}/>
        </HeaderButtons>)
})

const styles = StyleSheet.create({
    center: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    version: {
        fontFamily: 'open-bold'
    }
})
