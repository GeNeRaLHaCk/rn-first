import React, {useEffect} from "react";
import {HeaderButtons, Item} from 'react-navigation-header-buttons'
import {useDispatch, useSelector} from "react-redux";
import {ActivityIndicator, StyleSheet, View} from 'react-native'
import {Post} from "../components/Post";
import {AppHeaderIcon} from "../components/AppHeaderIcon";
import {PostList} from "../components/PostList";
import {loadPosts} from "../store/actions/post";
import {THEME} from "../theme";

export const MainScreen = ({navigation}) => {
    console.log("OPEN MAIN SCREEN2")
    const openPostHandler = (post) => {
        navigation.navigate('Post', {postId: post.id, date: post.date, booked: post.booked})
    }

    const dispatch = useDispatch()

    useEffect(() => {
        dispatch(loadPosts())
    }, [dispatch]) //будет вызываться без зависимостейф тогда, когда весь шаблон будет готов к работе
    //если в useEffect будет использоваться какие-то функции, нужно поставить в зависимости
    const allPosts = useSelector(state => state.post.allPosts) //позволяет получить доступ до state (функция с аргументом нашего statw-a)

    const loading = useSelector(state => state.post.loading)

    if(loading) {
        return (
            <View style={styles.center}>
                <ActivityIndicator color={THEME.MAIN_COLOR}/>
            </View>
        )
    }

    return (<PostList data={allPosts}
                      onOpen={openPostHandler}/>)
}

MainScreen.navigationOptions = ({navigation}) => ({ //мы не можем засунуть внутри объекта navigation, потому что это объект =>
    //превращаем в функцию
    headerTitle: 'Мой блог',
    headerRight: () =>
        (<HeaderButtons HeaderButtonComponent={AppHeaderIcon}>
            <Item title="Take photo"
                  iconName="ios-camera"
                  onPress={() => navigation.push('Create')}/>
        </HeaderButtons>),
    headerLeft: () =>
        (<HeaderButtons HeaderButtonComponent={AppHeaderIcon}>
            <Item title="Toggle Drawer"
                  iconName="ios-menu"
                  onPress={() => navigation.toggleDrawer()}/>
        </HeaderButtons>)
})

const styles = StyleSheet.create({
    center: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    }
})