import {createStore, combineReducers, applyMiddleware} from "redux";
import thunk from 'redux-thunk'
import {postReducer} from "./reducers/post";

const rootReducer = combineReducers({
    post: postReducer
})//преречисляем все редюсеры нашего приложеения

export default createStore(rootReducer, applyMiddleware(thunk)) //научили стор работать с асинхронными событиями(добавили второй параметр applyMiddleware)