import React, {useState} from "react";
import {View, StyleSheet, Button, TextInput, Alert, Keyboard} from "react-native";
import { AntDesign } from '@expo/vector-icons';

export const AddTodo = ({onSubmit}) => {
    const [value, setValue] = useState('')

    const pressHandler = () => {
        if (value.trim()) {
            onSubmit(value)
            setValue('')
            Keyboard.dismiss()
        } else {
            Alert.alert("Yo bro, vvedi cifri")
        }
    }

    return (
        <View style={styles.block}>
            <TextInput style={styles.input}
                       onChangeText={setValue}
                       value={value}
                       placeholder="ЦИФРЫ"
                       autoCorrect={false}
                       autoCapitalize='none'
                       keyboardType='number-pad'
            />
            <AntDesign.Button name='pluscircleo'
                              onPress={pressHandler}>
                Add
            </AntDesign.Button>
{/*            <Button title="Добавить"

                    onPress={pressHandler} //не вызываем (не пишем скобки), просто передаём референс на неё
            />*/}
        </View>
    )
}

const styles = StyleSheet.create({
    block: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginBottom: 15
    },
    input: {
        width:'70%',
        padding: 10,
        borderStyle: 'solid',
        borderBottomWidth: 2,
        borderBottomColor: '#3949ab'
    }
})