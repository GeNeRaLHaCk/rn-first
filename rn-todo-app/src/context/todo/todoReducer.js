import {
    ADD_TODO,
    CLEAR_ERROR,
    FETCH_TODOS,
    HIDE_LOADER,
    REMOVE_TODO,
    SHOW_ERROR,
    SHOW_LOADER,
    UPDATE_TODO
} from "../types";

const handlers = {
    [ADD_TODO]: (state, {title, id}) => ({
        ...state, todos: [...state.todos, {
            id: id,
            title
        }]
    }), // поменяем стейт добавив в него новый элемент}) // нужно обернуть в круглые скобки чтобы js понимал что здесь возвращается объект
    [REMOVE_TODO]: (state, {id}) => ({
        ...state,
        todos: state.todos.filter(x => x.id !== id)
    }),
    [UPDATE_TODO]: (state, {title, id}) => ({
        ...state, todos: state.todos.map(x => {
            if (x.id === id) {
                x.title = title
            }
            return x
        })
    }),
    [SHOW_LOADER]: (state) => ({...state, loading: true}),
    [HIDE_LOADER]: (state) => ({...state, loading: false}),
    [CLEAR_ERROR]: (state) => ({...state, error: null}),
    [SHOW_ERROR]: (state, {error}) => ({...state, error}),
    [FETCH_TODOS]: (state, {todos}) => ({...state, todos}),
    DEFAULT: state => state
}

//state - это объект, у которого пока есть только ключ todos
export const todoReducer = (state, action) => { //редюсер это обычная функция, которая работает со стейтом
    //нам нельзя мутировать стейт, нам нужно всегда возвращать новую ссылку на объект
    const handler = handlers[action.type] || handlers.DEFAULT
    return handler(state, action)
}