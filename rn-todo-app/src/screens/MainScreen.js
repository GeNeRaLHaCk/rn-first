import React, {useState, useEffect, useContext, useCallback} from 'react'
import {View, StyleSheet, FlatList, Dimensions} from 'react-native'
import {AddTodo} from "../components/AddTodo";
import {Todo} from "../components/Todo";
import {THEME} from "../theme";
import {TodoContext} from "../context/todo/todoContext";
import {ScreenContext} from "../context/screen/screenContext";
import {AppLoader} from "../components/ui/AppLoader";
import {AppText} from "../components/ui/AppText";
import {AppButton} from "../components/ui/AppButton";

export const MainScreen = () => {
    const {addTodo, todos, removeTodo, fetchTodos, loading, error} = useContext(TodoContext) // для этого рефактор был
    const {changeScreen} = useContext(ScreenContext) //импортируем здесь


    const [deviceWidth, setDeviceWidth] = useState(
        Dimensions.get('window').width - THEME.PADDING_HORIZONTAL * 2)

    const loadTodos = useCallback(async () => await fetchTodos(), [fetchTodos]) // 2 параметр - список зависимостей, передаём сам список fetchTodos
    //обёртка по сути реакта для оптимизации
    //выполняет наш fetchTodos и складывает в список зависимостей(fetchTodos)
    //избежим лишнего ререндеринга и выполнения метода
    useEffect(() => {
        loadTodos()
    }, [])

    useEffect(() => { //используем хук без 2 параметра, чтобы он запустился 1 раз при инициализации компонента
        const update = () => { //высчитывает от текущей ориентации ширину и меняет state
            const width =
                Dimensions.get('window').width - THEME.PADDING_HORIZONTAL * 2
            setDeviceWidth(width)
        }
        Dimensions.addEventListener('change', update) //1 раз добавляем прослушку события change(когда меняется экран), вызываем метод update
        //адаптивная вёрстка
        return () => { //когда происходит дестрой компонента, удаляем событие change и функцию update
            Dimensions.removeEventListener('change', update)
        }
    })

    if (loading) {
        return <AppLoader/>
    }

    if (error) {
        return (
            <View style={styles.center}>
                <AppText style={styles.error}>{error}</AppText>
                <AppButton>Retry</AppButton>
            </View>)
    }
    let content = (
        <View style={{width: deviceWidth}}>
            <FlatList data={todos}
                      keyExtractor={item => item.id.toString()}
                      renderItem={({item}) => (
                          <Todo todo={item}
                                onRemove={removeTodo}
                                onOpen={changeScreen}
                          />
                      )}/>
        </View>)

    if (todos.length === 0) {
        //require  встроенная ф-ия
        //из node js, которая позволяет какие -либо модули или сущности из других файлов

        content = <View style={styles.imgWrap}>

            {/*            <Image style={styles.image} source={require('../../assets/adaptive-icon.png')} />*/}
        </View>
    }

    return (
        <View>
            <AddTodo onSubmit={addTodo}/>
            {content}
        </View>
    )
}

const styles = StyleSheet.create({
    imgWrap: {
        alignItems: 'center',
        justifyContent: 'center',
        padding: 10,
        height: 300
    },
    image: {
        width: '100%',
        height: '100%',
        resizeMode: 'contain'
    },
    center: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    error: {
        fontSize: 20,
        color: THEME.DANGER_COLOR
    }
})