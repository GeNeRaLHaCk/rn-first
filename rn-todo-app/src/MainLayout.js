import React, {useContext} from "react";
import {View, StyleSheet} from "react-native";
import {Navbar} from "./components/NavBar";
import {THEME} from "./theme";
import {MainScreen} from "./screens/MainScreen";
import {TodoScreen} from "./screens/TodoScreen";
import {ScreenContext} from "./context/screen/screenContext"; //здесь мы будем манипулировать скринами(показывать шаблон)

export const MainLayout = () => { //лэйоут уже не должен перадавать в компоненты такие вещи как функции,
    // пропсы, он должен рендерить

    const {todoId} = useContext(ScreenContext)
    //const [todoId, setTodoId] = useState(null)

/*    const [todos, setTodos] = useState([]) //локальный стейт для приложения*/
    // тип данных кортеж ( два фиксированных значения,
    // (первый - стейт приложения
    //  второй - функция которая позволяет изменить этот стейт)

    return (
        <View style={styles.wrapper}>
            <Navbar title="Bro"/>
            <View style={styles.container}>
                {todoId ? <TodoScreen/> : <MainScreen />}
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        paddingHorizontal: THEME.PADDING_HORIZONTAL,
        paddingVertical: 20,
        flex: 1
    },
    wrapper: {
        flex: 1
    }
})
